exports.getMovies = () => {
    let movies = [
        {
            movie_theater: 'Cinepolis',
            movie: 'Frozen 2',
            time: '14:30-15:30'
        },
        {
            movie_theater: 'Cinemex',
            movie: 'Warcraft',
            time: '15:00-17:30'
        },
        {
            movie_theater: 'Cinemex',
            movie: 'F&F 8',
            time: '20:30-22:00'
        },
        {
            movie_theater: 'Cinepolis',
            movie: 'Shrek',
            time: '19:00-20:30'
        },
        {
            movie_theater: 'Cinepolis',
            movie: 'No manches frida 15',
            time: '12:00-23:30'
        }
    ];
    return movies;
}