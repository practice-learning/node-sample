express = require('express');
cors = require('cors');

let app = express();
app.use(cors());

let moviesHandler = require('./methods/moviesHandler');

app.get('/feed', (req, res) => {
    let movies = moviesHandler.getMovies();
    console.log(movies);
    res.status(200).json(movies);
});

app.listen(8000, () => {
    console.log("Listening on port 8000");
})

